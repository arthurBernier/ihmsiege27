const ipc = require('electron').ipcRenderer;
const electron = require('electron');


//End of requires
electron.webFrame.setVisualZoomLevelLimits(1,1)

$(document).ready(function()
{
  //Declarations

  var selectedDevice = [];
  var indexSelected = [];
  var existingTransaction = [];
  var selected =false;
  var allWindows = ["#idleContent","#borneContent","#shipOwnerContent","#authentificationContent","#waitingContent","#recapitulativContent","#adminAuthContent","#adminContent"];
  var actualWindow = 0;
  var timerId;
  var radialID;
  var codeSequence = "";
  var hideCodeSequence = "";
  var tempCode;
  var InitArg;
  var transactionID = null;
  var activityTimeout = setTimeout(inActive, 360000);
  var existTimer = false;
  var indexToIPC = [];
  var numberOfTimers = 0;
  var timersValidation = [];
  //End of declarations


  /////////////////////////// \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  /////////////////Multi-Language begin \\\\\\\\\\\\\\\\\\\\\\\\\
  ///////////////////////////////////////////////////////////////

  const frenchLanguage =
  { "idleString":"Bienvenue <br> Veuillez appuyer sur l'écran",
    "header_H1":"Accueil",
    "header_H3":"Sélectionnez le(s) point(s) de recharge souhaité(s)",
    "dispoLegend":"DISPONIBLE",
    "delayedLegend":"EN CHARGE",
    "reservedLegend": "INDISPONIBLE",
    "validationString": "Valider la sélection",
    "validationStringRecap": "Valider la transaction",
    "so_H1": "Choisissez votre armateur",
    "authInfoString": "Ou <br />Code",
    "recapInfoString": "Vous pouvez vous brancher",
    "waterDeliveryInfoString": "Appuyez sur le(s) bouton(s) pour activer la distribution",
    "warnFirst": "Vous avez dépassé le temps imparti pour vous brancher",
    "warnSecond": "Vous disposez de 5 minutes pour relancer le timer avant que la transaction ne soit annulée",
    "warnReload": "Relancer",
    "warnEnding": "Arrêt définitif",
    "warnCancel": "Annuler",
    "abort": "Annuler transaction",
    "infoStringModifying": "Appuyez sur les boutons pour changer leur état",
    "confirmFirst": "Transaction en cours",
    "endingFirst": "La transaction est désormais terminée",
    "endingSecond": "Passez une bonne journée",
    "adminDoor": "Ouvrir la porte",
    "adminCloseDoor": "Fermer la porte",
    "adminEnding": "Mettre fin à toutes les transactions",
    "adminHome": "Retourner à l'accueil",
    "wrongFirst": "Code inconnu, abandon",
    "confirmTitle": "Arrêt",
    "pausedTitle" : "Arrêt / Pause",
    "endingTitle" : "Fin de la transaction",
    "wrongTitle": "Echec",
    "noPlugtitle": "Attention",
    "type2Title": "Veuillez Patienter",
    "type2First": "Nous déverouillons la prise",
    "confirmSecond" : "Etes vous sur ?"
  };

  const englishLanguage =
  { "idleString":"Welcome <br> Please touch the screen",
    "header_H1":"Home",
    "header_H3":"Select your Charge Point(s)",
    "dispoLegend":"AVAILABLE",
    "delayedLegend":"CHARGING",
    "reservedLegend": "UNAVAILABLE",
    "validationString": "Validate the selection",
    "validationStringRecap": "Validate the transaction",
    "so_H1": "Choose your shipowner",
    "authInfoString": "Or <br />Code",
    "recapInfoString": "You can plug in",
    "waterDeliveryInfoString": "Touch the button(s) to activate distribution",
    "warnFirst": "You have exceeded the allotted time to plug in",
    "warnSecond": "You have  5 minutes to reload the timer before the transaction be canceled",
    "warnReload": "Reload",
    "warnEnding": "Final Shutdown",
    "warnCancel": "Cancel",
    "abort": "Cancel transaction",
    "infoStringModifying": "Touch the button(s) to change their state",
    "confirmFirst": "Transaction",
    "confirmSecond":"are you sure ?",
    "endingFirst": "The transaction is now finished",
    "endingSecond": "Have a good day",
    "adminDoor": "Open the door",
    "adminCloseDoor": "Close the door",
    "adminEnding": "Ending all the transactions",
    "adminHome": "Return Home",
    "wrongFirst": "Wrong code, abort",
    "confirmTitle": "Stop",
    "pausedTitle" : "Stop / Pause",
    "endingTitle" : "Transaction end",
    "wrongTitle": "Fail",
    "noPlugtitle": "Warning",
    "type2Title": "Please Wait",
    "type2First": "We unlock the connector"
  };

  const germanLanguage =
  { "idleString":"Willkommen <br> Bitte betaetigen Sie den Bildschirm",
    "header_H1":"Startseite",
    "header_H3":"Wählen Sie bitte den oder die Punkte zum Aufladen aus",
    "dispoLegend":"BETRIEBSBEREIT",
    "delayedLegend":"IM LADEZUSTAND",
    "reservedLegend": "NICHT VERFÜGBAR",
    "validationString": "Auswahl bestaetigen",
    "validationStringRecap": "Auswahl bestaetigen",
    "so_H1": "Wählen Sie Ihr Wasserfahrzeug aus",
    "authInfoString": "Oder <br />Code",
    "recapInfoString": "Sie können sich jetzt andocken",
    "waterDeliveryInfoString": "Druecken Sie bitte eine Taste um das Aufladen zu aktivieren ",
    "warnFirst": "Sie haben die Zeit ueberschritten um sich anzudocken",
    "warnSecond": "Sie haben noch 5 Minuten Zeit, um den Zähler neu zu starten",
    "warnReload": "Erneut versuchen",
    "warnEnding": "Endgueltiger Abbruch",
    "warnCancel": "Abbrechen",
    "abort": "Vorgang abbrechen",
    "infoStringModifying": "Druecken Sie eine Taste um den Status zu aendern",
    "confirmFirst": "Vorgang in Bearbeitung",
    "confirmSecond":"Bist du sicher ?",
    "endingFirst": "Der Vorgang ist nunmehr beendet",
    "endingSecond": "Wir wünschen Ihnen einen schönen Tag",
    "adminDoor": "Tuer oeffnen",
    "adminCloseDoor": "Tor zu öffnen",
    "adminEnding": "Alle Vorgaenge beenden",
    "adminHome": "Zurueck zur Startseite",
    "wrongFirst": "Unbekannter Code, Aufgabe",
    "confirmTitle": "Stoppen",
    "pausedTitle" : "Erholung / Stoppen",
    "wrongTitle": "Versagen",
    "endingTitle" : "Ende",
    "noPlugtitle": "Achtung",
    "type2Title": "Bitte warte",
    "type2First": "Wir entsperren den Haken"
  };

  const spanishLanguage =
  { "idleString":"Bienvenido <br>Toque la pantalla para continuar",
    "header_H1":"Inicio",
    "header_H3":"Seleccione el (los) punto(s) de recarga deseado(s)",
    "dispoLegend":"DISPONIBLE",
    "delayedLegend":"EN CARGA",
    "reservedLegend": "INDISPONIBLE",
    "validationString": "Validar la selección",
    "validationStringRecap": "Validar la transacción",
    "so_H1": "Escoja su Compañia de Navegación",
    "authInfoString": "O <br />Código",
    "recapInfoString": "Puede ahora conectarse",
    "waterDeliveryInfoString": "Presione el(los) botone(s) para activar la distribución",
    "warnFirst": "Ha depasado el tiempo dado para conectarse",
    "warnSecond": "Tienes 5 minutos para volver a cargar el temporizador antes de cancelar la transacción", //***
    "warnReload": "Reinicio",
    "warnEnding": "Parar definitivamente",
    "warnCancel": "Anular",
    "abort": "Anular la transacción",
    "infoStringModifying": "Apoye los botones para cambiar su estado",
    "confirmFirst": "Transacción en curso",
    "confirmSecond":"Está usted seguro ?",
    "endingFirst": "La transacción ha sido finalizada",
    "endingSecond": "Que tenga buen dia",
    "adminDoor": "Abrir la puerta",
    "adminCloseDoor": "Cerrar la puerta",
    "adminEnding": "Terminar todas las transacciones",
    "adminHome": "Regresar al inicio",
    "wrongFirst": "Código incorrecto",
    "confirmTitle": "Detener",
    "pausedTitle" : "Detener / Pausa",
    "endingTitle" : "Ende",
    "wrongTitle": "Fracaso",
    "noPlugtitle": "Atención",
    "type2Title": "Espere por favor",
    "type2First": "Desbloqueamos el conector"
  };

  var selectedLanguage = frenchLanguage;

  //Change language and css value to fit to the words
  function changeLanguage(language)
  {
    switch (language) {
      case "French":
          selectedLanguage = frenchLanguage;
          $('#idleString').css('left', '');
          $('#validationString').css('left', '');
          $('#validationStringRecap').css('left', '');
          $('#recapInfoString').css('left', '');
          $('#recapInfoString').css('width', '');
          $('#waterDeliveryInfoString').css('left', '');
          $('#waterDeliveryInfoString').css('width', '');
          $('#infoStringModifying').css('left', '');
          $('#dialog-confirm').prop('title', 'Arrêt');
          $('#dialog-paused').prop('title', 'Reprise / Arrêt');
          $('#dialog-wrong-code').prop('title', 'Echec');
          $('#explication > fieldset > label').css('margin-right', '');
          $('.explicationButton').css('margin-left', '');
        break;
      case "English":
          selectedLanguage = englishLanguage;
          $('#idleString').css('left', '4.5em');
          $('#validationString').css('left', '1.5em');
          $('#validationStringRecap').css('left', '1em');
          $('#recapInfoString').css('left', '6.5em');
          $('#recapInfoString').css('width', '');
          $('#waterDeliveryInfoString').css('left', '3em');
          $('#waterDeliveryInfoString').css('width', '19em');
          $('#infoStringModifying').css('left', '8.5em');
          $('#dialog-confirm').prop('title', 'Stop');
          $('#dialog-wrong-code').prop('title', 'Fail');
          $('#dialog-paused').prop('title', 'Resumption / Stop');
          $('#explication > fieldset > label').css('margin-right', '');
          $('.explicationButton').css('margin-left', '');
        break;
      case "German":
          selectedLanguage = germanLanguage;
          $('#idleString').css('left', '2.5em');
          $('#header_H1').css('margin-left', '3.5em');
          $('#explication > fieldset > label').css('margin-right', '1.6em');
          $('.explicationButton').css('margin-left', '-2.4em');
          $('#validationString').css('left', '1.5em');
          $('#validationStringRecap').css('left', '1.5em');
          $('#recapInfoString').css('left', '3.5em');
          $('#recapInfoString').css('width', '13em');
          $('#waterDeliveryInfoString').css('left', '3em');
          $('#waterDeliveryInfoString').css('width', '19em');
          $('#infoStringModifying').css('left', '7.5em');
          $('#dialog-confirm').prop('title', 'Stoppen');
          $('#dialog-paused').prop('title', 'Erholung / Stoppen');
          $('#dialog-wrong-code').prop('title', ' Versagen ');
        break;
      case "Spanish":
          selectedLanguage = spanishLanguage;
          $('#idleString').css('left', '');
          $('#validationString').css('left', '');
          $('#validationStringRecap').css('left', '1.5em');
          $('#recapInfoString').css('left', '4.5em');
          $('#recapInfoString').css('width', '');
          $('#waterDeliveryInfoString').css('left', '');
          $('#waterDeliveryInfoString').css('width', '');
          $('#infoStringModifying').css('left', '8.5em');
          $('#dialog-confirm').prop('title', 'Detener');
          $('#dialog-paused').prop('title', 'Reanudación / Detener');
          $('#dialog-wrong-code').prop('title', 'Fracaso');
          $('#dialog-wrong-code').prop('title', 'Fracaso');
          $('#explication > fieldset > label').css('margin-right', '');
          $('.explicationButton').css('margin-left', '');
        break;
      default:
          selectedLanguage = frenchLanguage;
    }
    initLanguage();
  }

  //Initiate the selected language with all the text area
  function initLanguage()
  {
    $('#idleString').html(selectedLanguage["idleString"]);
    $('#header_H1').html(selectedLanguage["header_H1"]);
    $('#header_H3').html(selectedLanguage["header_H3"]);
    $('#dispoLegend').html(selectedLanguage["dispoLegend"]);
    $('#delayedLegend').html(selectedLanguage["delayedLegend"]);
    $('#reservedLegend').html(selectedLanguage["reservedLegend"]);
    $('#validationString').html(selectedLanguage["validationString"]);
    $('#validationStringRecap').html(selectedLanguage["validationStringRecap"]);
    $('#so_H1').html(selectedLanguage["so_H1"]);
    $('#authInfoString').html(selectedLanguage["authInfoString"]);
    $('#recapInfoString').html(selectedLanguage["recapInfoString"]);
    $('#waterDeliveryInfoString').html(selectedLanguage["waterDeliveryInfoString"]);
    $('#warnFirst').html(selectedLanguage["warnFirst"]);
    $('#warnSecond').html(selectedLanguage["warnSecond"]);
    $('#abort').html(selectedLanguage["abort"]);
    $('#infoStringModifying').html(selectedLanguage["infoStringModifying"]);
    $('#confirmFirst').html(selectedLanguage["confirmFirst"]);
    $('#confirmSecond').html(selectedLanguage["confirmSecond"]);
    $('#pausedFirst').html(selectedLanguage["confirmFirst"]);
    $('#endingFirst').html(selectedLanguage["endingFirst"]);
    $('#wrongFirst').html(selectedLanguage["wrongFirst"]);
    $('#endingSecond').html(selectedLanguage["endingSecond"]);
    $('#adminDoor').html(selectedLanguage["adminDoor"]);
    $('#adminEnding').html(selectedLanguage["adminEnding"]);
    $('#adminHome').html(selectedLanguage["adminHome"]);
    $('#type2First').html(selectedLanguage["type2First"]);
    applyChangesToContent();
  }

  changeLanguage("French");

  //Manage the display of the language selection
  $(".header_flag").click(function(element){
    var oldPlace;
    var newPlace;
    var idClicked =  $(this).attr("id");
    newPlace = $(this).css("top");
    if ($(".backGroundDiv").hasClass('show'))
    {
        $( ".header_flag" ).each(function() {
        if ($(this).hasClass("selectedLanguage"))
         {
           if ($(this).attr("id") != idClicked) {
             oldPlace = $(this).css("top");
             $(this).css("top",newPlace);
             $(this).removeClass("selectedLanguage");
             $(this).addClass("unselectedLanguage");
           }
          }
          else {
            $(this).addClass("unselectedLanguage");
          }
        });
      $(this).css("top",oldPlace);
      $(this).addClass("selectedLanguage");
      $(this).removeClass("unselectedLanguage");
      $(".backGroundDiv").removeClass('show');
      $("#downArrow").css("top","");
      $("#downArrow").css("transform","");
      changeLanguage($(this).attr("value"));
    }
    else {
      $(".backGroundDiv").addClass('show');
      $(".backGroundDiv").one("transitionend", function(){
        // $("#downArrow").css("top","8em");
        $("#downArrow").css("transform","rotate(180deg)");
        $( ".header_flag" ).each(function() {
        if ($(this).hasClass("unselectedLanguage"))
         {
          $(this).removeClass("unselectedLanguage");
          }
        });
       });
    }
  });

  /////////////////////////// \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  /////////////////Multi-Language ending \\\\\\\\\\\\\\\\\\\\\\\\\
  ///////////////////////////////////////////////////////////////

  //Reset the timer for inactivity
  function resetActive(){
      clearTimeout(activityTimeout);
      activityTimeout = setTimeout(inActive, 360000);
  }

  // Check for mousemove, could add other events here such as checking for key presses ect.
  $(document).bind('mousemove', function(){resetActive()});

  // No activity do something.
  function inActive(){
    $("#mainContent").css("height", "100%");
    if ($('.clicked').length != 0) {
      validateTransaction();
    }
      changeWindowContent("idle");
  }

  //Add to codesequence the value of the ship owner
  $('#shipOwnerContent').children().on('click', function (e) {
    $(this).addClass("inActive");
     if ($(this).attr("id") == "nicols") {
       tempCode = "2424";
     }
  });

  //Toggle to the admin mode
  $("#sga_Logo").click(function(){
    $(".previousArrow").removeClass("inActive");
    changeWindowContent("admin");
  });


  //If a click is detected on the idle Menu
  //We change window to next Menu
  $("#mainContent").click(function(){
    if (actualWindow == 0) {
      $("#mainContent").css("height","0%");
      changeWindowContent("next");
    }
  });

  //Toggle to authentification menu
  $(".soButtons").click(function(){
      changeWindowContent("next");
  });

  //Function that highlight an object
  //Take three parameters : the object ID , the first and second color to apply
  //The color changement take 1 second, change the 1000 value to rise or down it
  function highLightObject(object,firstColor,secondColor)
  {
    if ($("#"+object ).css("background-color") == hexToRGB(secondColor)) {
      $("#"+ object ).animate({
    backgroundColor: firstColor
  }, 1000, function() {
    highLightObject(object,firstColor,secondColor);
  });
    }
    else {
      $( "#"+object ).animate({
    backgroundColor: secondColor
  }, 1000, function() {
    highLightObject(object,firstColor,secondColor);
  });
    }
  }

  //Function that transform a hexadecimal color value in RGB value
  //Used with highLightObject()
  //Take only the hex string
  function hexToRGB(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    var rgbResult = "rgb(";
    if(result)
      rgbResult += parseInt(result[1], 16) + ", " + parseInt(result[2], 16) + ", " +  parseInt(result[3], 16) + ")";
    else
      rgbResult = null
    return rgbResult;
}

  //Function that manage the water dropping animation
  //Take the id of the drop of water
/*  function waterDroping(dropID)
  {
    var divName = $(dropID).closest("div").attr("id");
    var tapName = $("#"+divName).find('.tap').attr("id");
    $("#"+tapName).removeClass("hidden");
    $(dropID).css("width","2em");
    $(dropID).css("height","2em");
    if (dropID == "#firstDrop") {
      $(dropID).css("top","5em");
      $(dropID).css("left","0.5em");
    }
    else {
      $(dropID).css("top","5em");
      $(dropID).css("left","3.5em");
    }
    $(dropID).animate({
      top: "+=13.5em",
    }, 800, function() {
        waterDroping(dropID);
    });
  }*/

  //Buttons actions
  $( ".type2,.domestic" ).click(function(object)
  {
    if ($(this).closest("div").hasClass("charging"))
    {
      transactionID = $(this).closest("div").attr("id");
      for (var i = 0; i < existingTransaction.length; i++) {
        if ($.inArray(transactionID,existingTransaction[i]) != -1) {
          transactionID = i;
        }
      }
      warningConfirm(existingTransaction[transactionID],$(this).closest("div").attr("id"));
    }
    else if ($(this).closest("div").hasClass("selected") && actualWindow != 5)
    {
      $(this).closest("div").removeClass("selected");
      for (var i = 0; i < selectedDevice.length; i++) {
        if(selectedDevice[i] == $(this).closest("div").attr("id"))
          remove(selectedDevice,$(this).closest("div").attr("id"));
      }
      remove(indexSelected,$(this).closest("div").index("#iconsBorne > div"));
    }
    else if ($(this).closest("div").hasClass("selected") && actualWindow == 5)
    {
      $(this).closest("div").stop();
      $(this).closest("div").finish();
      $(this).closest("div").css("background-color","#0abfef");
      $(this).closest("div").addClass("clicked");

      //We send it to the back-end
      ipc.send('launchDelivery', $(this).closest("div").index());
      // waitTimerValidation(5,$(this).closest("div").attr("id"));
      if ($('.clicked').length == selectedDevice.length) {
          $(this).addClass("hidden");
          var checkMark = $(this).closest("div").find('.check').attr("id");
          $("#"+checkMark).removeClass("hidden");
          highLightObject($(this).closest("div").attr("id"),"#0abfef","#ccd100");
          $("#validateTransactionButton").css('visibility','visible').hide().fadeIn('slow');
          $('#radialTimer > canvas').remove();
          existTimer = false;
          clearInterval(timerId);
          clearInterval(radialID);
      }
      else {
        $(this).addClass("hidden");
        var checkMark = $(this).closest("div").find('.check').attr("id");
        $("#"+checkMark).removeClass("hidden");
        highLightObject($(this).closest("div").attr("id"),"#0abfef","#ccd100");
      }
      $("#abort").css('visibility','visible').hide().fadeOut('slow');
    }
    else if ($(this).closest("div").hasClass("unavailable")) {}
    else if ($(this).closest("div").hasClass("paused"))
    {
      transactionID = $(this).closest("div").attr("id");
      for (var i = 0; i < existingTransaction.length; i++) {
        if ($.inArray(transactionID,existingTransaction[i]) != -1) {
          transactionID = i;
        }
      }
    }
    else
    {
      if (!getIfOpposite($(this).closest("div").index("#iconsBorne > div"))) {
        $(this).closest("div").addClass("selected");
        selectedDevice.push($(this).closest("div").attr("id"));
        indexSelected.push($(this).closest("div").index("#iconsBorne > div"));
      }
    }
    if ($(".selected")[0])
    {
      if (!selected)
      {
        $("#validationSelectionButton").css('visibility','visible').hide().fadeIn('slow');
        selected = true;
      }
    }
    else {
        $("#validationSelectionButton").fadeOut('1000');
        selected = false;
    }
  });

  //Get if opposite of this div exist
  //Used to know if just one side of the terminal is selected
  function getIfOpposite(index)
  {
    if (indexSelected.length==1){
      return true;
    }
  /*  for (var i = 0; i < indexSelected.length; i++) {
      if (index > 1)
      {
        if (indexSelected[i] <=1)
        {
          return true;
        }
      }
      else if (index <= 1)
      {
        if (indexSelected[i] > 1)
        {
          return true;
        }
      }
    }*/
  }

  //Function that transform an item, make it visible, or invisible
  //Take the item id on parameter
  function changeAppearance(item)
  {
    if ( $(item).hasClass("hidden"))
    {
      $(item).removeClass("hidden");
    }
    else
    {
      $(item).addClass("hidden");
    }
  }

  //Toggle to authentification menu when the validation button is pressed
  $("#validationSelectionButton").click(function()
  {
    $("#languageSelector").css("visibility", "hidden");
    $(this).addClass("inActive");
    $(this).addClass("hidden");
    $(".previousArrow").removeClass("inActive");
    changeWindowContent("authentification");
  });

  //Erase the last occruence in a code sequence
  $(".backWardButtonClass").click(function()
  {
    if (actualWindow == 6) {
      codeSequence = codeSequence.slice(0, -1);
      hideCodeSequence = hideCodeSequence.slice(0, -1);
      $("#currentCodeAdmin").text(hideCodeSequence);
    }
    else {
      codeSequence = codeSequence.slice(0, -1);
      $("#currentCode").text(codeSequence);
    }

    if (codeSequence.length == 0) {
      $(".explicativeIcons").each(function(index)
      {
        $(this).removeClass("hidden");
      });
    }
  });

  //Toggle to previous menu
  $( ".previousArrow" ).click(function()
  {
    codeSequence = "";
    hideCodeSequence = "";
    $("#currentCode").text("");
    $("#currentCodeAdmin").text("");
    $("#validationSelectionButton").removeClass("inActive");
    $('#shipOwnerContent').children().removeClass("inActive");
    $(this).addClass("inActive");
    $(".explicativeIcons").each(function(index)
    {
      $(this).removeClass("hidden");
    });
    transactionID = null;
    if (actualWindow == 6)
      changeWindowContent("init");
    else
    changeWindowContent("init");
  });

  //Test button to simulate validation/wiring
  $("#validateTransactionButton").click(function()
  {
    validateTransaction();
  });

  //Verificate the input code, if the code is knew
  //We go through, else we display an error message
  //And we go back to the authentification
  $(".authValidateButtonClass").click(function()
  {
    $( ".previousArrow" ).removeClass("inActive");
    if (codeSequence.length != 0)
    {
      if (actualWindow == 6) {
        if (codeSequence == "#*#") {
          ipc.send("enterAdmin", new Date($.now()), codeSequence);
          changeWindowContent("adminMode");
          codesequence="";
          hideCodeSequence = "";
          $("#currentCodeAdmin").text("");
          if (existingTransaction.length == 0) {
            $("#endingCircle").css("background", "gray");
          }
          else {
            $("#endingCircle").css("background", "");
          }
        }
      }
      else
      {
        // ipc.send("checkCode", codeSequence);
        changeWindowContent("next");
        ipc.send("tmpCode",codeSequence);
        waitingScreen();
      }
    }
  });

  //Button that open or close the terminal door
  //(Come in admin mode)
  $("#doorCircle").click(function()
  {
    if ($("#doorLock").hasClass("opened")) {
      ipc.send("door",0);
      $('#doorCircle > h1').text(selectedLanguage["adminDoor"]);
      $("#doorLock").attr("src","../Visuels/Logo/lock.png");
      $("#doorLock").removeClass("opened");
    }
    else {
      $('#doorCircle > h1').text(selectedLanguage["adminCloseDoor"]);
      ipc.send("door",1);
      $("#doorLock").attr("src","../Visuels/Logo/open_lock.png");
      $("#doorLock").addClass("opened");
    }
  });

  //Button that end all the transactions
  //(Come in admin mode)
  $("#endingCircle").click(function()
  {
    while (existingTransaction.length != 0) {
      for (var j = 0; j < existingTransaction[0].length; j++) {
        endTransaction(existingTransaction[0],existingTransaction[0][j])
      }
    }
  });

  //Button that make a return to home menu
  //(Come in admin mode)
  $("#homeCircle").click(function()
  {
    changeWindowContent("init");
  });

  //Detect a keyboard button entry
  //Add this entry to codeSequence
  $( ".keyBoardButtons" ).click(function()
  {
    if (codeSequence.length <= 8) {
      codeSequence += $(this).val();
      hideCodeSequence += "*";
      $(".explicativeIcons").each(function(index)
      {
        $(this).addClass("hidden");
      });
      if (actualWindow ==6)
        $("#currentCodeAdmin").text(hideCodeSequence);
      else
        $("#currentCode").text(codeSequence);
    }
  });

  //Abort transaction before delivery of anything
  $("#abort").click(function()
  {
    $("#validationSelectionButton").removeClass("inActive");
    $('#radialTimer > canvas').remove();
    existTimer = false;
    clearInterval(timerId);
    clearInterval(radialID);
    changeWindowContent("init");
  });

  //Button that make a return since the ship owner menu
  $("#previousShipOwner").click(function(){
    $("#languageSelector").css("visibility", "");
    changeAppearance("#validationSelectionButton");
  });

  //Wait for the reponse of the server for the code enter by user
  //Remove the listener after reception of the information
  function waitingScreen()
  {
    const checked = () => {
      for (var i = 0; i < selectedDevice.length; i++) {
        indexToIPC.push($("#"+selectedDevice[i]).closest("div").index());
      }
      ipc.send("saction", indexToIPC);
      indexToIPC = [];
    changeWindowContent("next");
    ipc.removeListener("checkedCode", checked);
    ipc.removeListener("uncheckedCode", unchecked);
  };

  const unchecked = () => {
   warningWrongCode();
  ipc.removeListener("uncheckedCode", unchecked);
  ipc.removeListener("checkedCode", checked);
};
  ipc.on('checkedCode', checked);
  ipc.on('uncheckedCode', unchecked);
  }

  // function waitTimerValidation(timetoWait,element)
  // {
  //   var intervalID = window.setInterval(function () {
  //     timetoWait--;
  //     if (timetoWait == 0) {
  //       console.log("Finish");
  //       clearInterval(  $("#"+element).attr("data-timer-id"));
  //       }
  //   }, 1000);
  // $("#"+element).attr("data-timer-id",intervalID);
  // }

  function applyChangesToContent()
  {
    //We apply changes to the page
    switch (actualWindow)
    {
      case 1:
        $("#contentHeader").removeClass("hidden");
        $('#header_H1').html(selectedLanguage["header_H1"]);
        $("#header_H1").css("margin-left","4.5em");
        if (selectedLanguage == frenchLanguage) {
          $("#header_H3").text("Sélectionnez le point de recharge souhaité");
        }
        else if (selectedLanguage == englishLanguage) {
          $("#header_H3").text("Select your Charge Point(s)");
        }
        else if (selectedLanguage == germanLanguage) {
          $("#header_H1").css("margin-left","3.5em");
          $("#header_H3").text("Wählen Sie bitte den oder die Punkte zum Aufladen aus");
        }
        else if (selectedLanguage == spanishLanguage) {
          $("#header_H3").text("Seleccione el (los) punto(s) de recarga deseado(s)");
        }
        break;
      case 2:
          $("#contentHeader").addClass("hidden");
        break;
      case 3:
        $("#contentHeader").removeClass("hidden");
        $("#header_H1").text("Authentification");
        $("#header_H1").css("margin-left","0.5em");
        if (selectedLanguage == frenchLanguage) {
          $("#header_H3").text("Passez votre badge/carte sans contact");
        }
        else if (selectedLanguage == englishLanguage) {
          $("#header_H3").text("Pass your contactless badge/card");
        }
        else if (selectedLanguage == germanLanguage) {
          $("#header_H1").text("Beglaubigung");
          $("#header_H1").css("margin-left","2em");
          $("#header_H3").text("Übergeben Sie Ihr kontaktloses Abzeichen");
        }
        else if (selectedLanguage == spanishLanguage)
        {
          $("#header_H1").text("Autenticación");
            $("#header_H1").css("margin-left","1.5em");
          $("#header_H3").text("Pasa tu insignia/tarjeta sin contacto");
        }
        break;
      case 4:
        $("#header_H1").css("margin-left","2.5em");
        if (selectedLanguage == frenchLanguage) {
          $("#header_H1").text("Chargement");
          $("#header_H3").text("Veuillez patienter, nous interrogeons la base de donnée");
        }
        else if (selectedLanguage == englishLanguage) {
          $("#header_H1").text("Loading");
          $("#header_H3").text("Please wait, we request our data base");
        }
        else if (selectedLanguage == germanLanguage) {
          $("#header_H1").text("Laden");
          $("#header_H1").css("margin-left","5em");
          $("#header_H3").text("Bitte warten Sie");
        }
        else if (selectedLanguage == spanishLanguage)
        {
          $("#header_H1").text("Cargando");
          $("#header_H3").text("Espere por favor, consultamos la base de datos");
        }
        break;
      case 5:
      $("#header_H1").css("margin-left","2.5em");
      $("#header_H1").text("");
      if (selectedLanguage == frenchLanguage) {
          $("#header_H3").text("Branchez vous au connecteur selectionné");
        }
        else if (selectedLanguage == englishLanguage) {
          $("#header_H3").text("You can connect to the connector");
        }
        else if (selectedLanguage == germanLanguage) {
          $("#header_H3").text("Sie können eine Verbindung mit dem Connector herstellen");
          $("#header_H3").css("margin-top","0em");
        }
        else if (selectedLanguage == spanishLanguage)
        {
          $("#header_H3").text("Puedes conectarte al conector");
        }
        $("#infoStringModifying").addClass("hidden");
        $("#recapitulativContent").append($("#completeBorne"));
        $( "#validationSelectionButton" ).css("display", "none");
        $("#abort").css('visibility','visible').hide().fadeIn('slow');
        $("#validateTransactionButton").css('visibility','visible').hide().fadeOut('slow');
        displaySelectedContent();
        break;
      case 6:
        $("#header_H1").text("Admin");
        $("#header_H1").css("margin-left","4.5em");
        if (selectedLanguage == frenchLanguage) {
          $("#header_H3").text("Accès au mode administrateur");
        }
        else if (selectedLanguage == englishLanguage) {
          $("#header_H3").text("Access to administrator mode");
        }
        else if (selectedLanguage == germanLanguage) {

          $("#header_H3").text("Zugriff auf den Administratormodus");
        }
        else if (selectedLanguage == spanishLanguage)
        {
          $("#header_H3").text("Acceso al modo de administrador");
        }
        break;
      case 7:
        $("#header_H1").text("Admin");
        $("#header_H1").css("margin-left","4.5em");
        if (selectedLanguage == frenchLanguage) {
          $("#header_H3").text("Accès au mode administrateur");
        }
        else if (selectedLanguage == englishLanguage) {
          $("#header_H3").text("Access to administrator mode");
        }
        else if (selectedLanguage == germanLanguage) {

          $("#header_H3").text("Zugriff auf den Administratormodus");
        }
        else if (selectedLanguage == spanishLanguage)
        {
          $("#header_H3").text("Acceso al modo de administrador");
        }
        break;
    }
  }

  //Function to change content of the main window
  function changeWindowContent(direction, optionnal)
  {
    optionnal = optionnal || 0;
    if (direction == "next")
    {
      $(allWindows[actualWindow]).fadeOut( "2000" );
      actualWindow++;
      $(allWindows[actualWindow]).fadeIn( "2000" );
    }
    else if (direction == "previous")
    {
      $(allWindows[actualWindow]).fadeOut( "2000" );
      actualWindow--;
      $(allWindows[actualWindow]).fadeIn( "2000" );
    }
    else if (direction == "idle")
    {
      $(allWindows[actualWindow]).fadeOut( "2000" );
      $("#contentHeader").addClass("hidden");
      InitializingMainPage("none");
      actualWindow = 0;
      $(allWindows[actualWindow]).fadeIn( "2000" );
    }
    else if (direction == "admin") {
      $(allWindows[actualWindow]).fadeOut( "2000" );
      actualWindow = 6;
      $(allWindows[actualWindow]).fadeIn( "2000" );
    }
    else if (direction == "adminMode") {
      $(allWindows[actualWindow]).fadeOut( "2000" );
      actualWindow = 7;
      $(allWindows[actualWindow]).fadeIn( "2000" );
    }
    else if(direction =="authentification"){
      $(allWindows[actualWindow]).fadeOut("2000");
      actualWindow=3;
      $(allWindows[actualWindow]).fadeIn("2000");
    }
    else if (direction == "init") {
      $(allWindows[actualWindow]).fadeOut( "2000" );
      if (actualWindow == 1 && optionnal != 0 || actualWindow == 7 && optionnal != 0) {
        InitArg = "modifying";
      }
      else if (actualWindow == 5 && optionnal == 0) {
        InitArg = "validateToIdle";
      }
      else {
        InitArg = "none";
      }
      actualWindow = 1;
      $(allWindows[actualWindow]).fadeIn( "2000" );
      InitializingMainPage(InitArg);
    }
    applyChangesToContent();
  }

  //Reinitializing to MainPage
  function InitializingMainPage(arg)
  {
    if (arg == "validateToIdle") {
      if ($("#validateTransactionButton").css("visibility","visible")) {
        $("#validateTransactionButton").css("display","none");
        $("#validateTransactionButton").removeClass("inActive");
      }
      $("#iconsBorne > div" ).each(function(index)
      {
        if($(this).closest("div").hasClass("clicked"))
        {
          ipc.send("stopDelivery", $(this).index());
        }

        if ($(this).hasClass("selected")) {
          $(this).removeClass("selected");
          $(this).stop();
          $(this).css("background-color","");
          if ($(this).closest("div").attr("id") == "fifthCircle")
          {
            $("#secondDrop").stop();
            $("#secondDrop").finish();
            $("#secondDrop").css({ 'top' : '', 'left' : '' , 'width' : '', 'height': ''});
            $("#secondTap").addClass("hidden");
          }
          else if ($(this).closest("div").attr("id") == "sixthCircle")
          {
            $("#firstDrop").stop();
            $("#firstDrop").finish();
            $("#firstDrop").css({ 'top' : '', 'left' : '' , 'width' : '', 'height': ''});
            $("#firstTap").addClass("hidden");
          }
          else {
            $(this).children(".type2").removeClass("hidden");
            $(this).children(".domestic").removeClass("hidden");
            $(this).children(".check").addClass("hidden");
          }
        }
      });
    }

    $("#languageSelector").css("visibility", "");
    $("#borneContent").append($("#completeBorne"));
    $("#validationSelectionButton").css("display","none");
    $("#validationSelectionButton").removeClass("inActive");
    $("#nicols").removeClass("inActive");
    $("#slogan").removeClass("hidden");
    $("#iconsBorne > div" ).each(function(index)
    {
      $(this).removeClass("hidden");
      if ($(this).hasClass("selected")) {
        $(this).removeClass("selected");
        $(this).stop();
        $(this).css("background-color","");
      }
    });
    $("#completeBorne > img" ).each(function(index)
    {
      $(this).removeClass("hidden");
    });
    $("#explication > fieldset" ).each(function(index)
    {
      $(this).removeClass("hidden");
    });
    $(".clicked" ).each(function(index)
    {
      $(this).removeClass("clicked");
    });
    $(".explicativeIcons").each(function(index)
    {
      $(this).removeClass("hidden");
    });
    codeSequence = "";
    hideCodeSequence = "";
    $("#currentCode").text("");
    if (arg == "modifying") {
      existingTransaction.splice(transactionID,1);
    }
    selected = false;
    selectedDevice = [];
    indexSelected = [];
    if (existingTransaction.length > 0) {
      $("#infoStringModifying").removeClass("hidden");
    }
    else {
        $("#infoStringModifying").addClass("hidden");
    }
  }

  //Function to display the selected content part of the recapitulative page
  function displaySelectedContent()
  {
    $("#iconsBorne > div" ).each(function(index)
    {
      $(this).addClass("hidden");
    });

    //Hide elements for more commodity
    $("#firstBolt").addClass("hidden");
    $("#secondBolt").addClass("hidden");
    $("#explication > fieldset").addClass("hidden");
    $("#slogan").addClass("hidden");

    //We iterate to each div in the iconsBorne div, and if
    //we find a "selected" div, we remove "hidden" class, to display her
    $("#iconsBorne > div" ).each(function(index)
    {
      if ($(this).hasClass("selected"))
      {
        highLightObject($(this).attr("id"),"#ff6800","#ffffff");
        $(this).removeClass("hidden");
        //We set visible links and icons correspondent to
        //the div
        switch ($(this).attr("id"))
        {
          case "firstCircle":
            $("#firstHorLink").removeClass("hidden");
            $("#firstVertLink").removeClass("hidden");
            $("#firstBolt").removeClass("hidden");
            break;
          case "secondCircle":
            $("#firstHorLink").removeClass("hidden");
            $("#thirdVertLink").removeClass("hidden");
            $("#firstBolt").removeClass("hidden");
            break;
          case "thirdCircle":
            $("#thirdHorLink").removeClass("hidden");
            $("#secondVertLink").removeClass("hidden");
            $("#secondBolt").removeClass("hidden");
            break;
          case "fourthCircle":
            $("#thirdHorLink").removeClass("hidden");
            $("#fourthVertLink").removeClass("hidden");
            $("#secondBolt").removeClass("hidden");
            break;
          default:
        }
      }
    });
    //We initialize the timer for the cable plug
    setTimer();
}

  //Setting radial timer to indicate the remaninig time to plug-in
  function setTimer()
  {
    if (!existTimer) {
      existTimer = true;
      var timerValue = 60;
      var timerToZero = false;
      $('#radialTimer').radialIndicator({
          barColor: '#9ae355',
          barWidth: 10,
          maxValue: 60,
          minValue: 0,
          initValue: 60,
          roundCorner : true
      });
      //We initialize a radialObj with the value of the data inside #radialTimer
      var radialObj = $('#radialTimer').data('radialIndicator');

      //For each second, we subtract 1 to the maxValue of #radialTimer
      //If timerValue == 0, we removed the canvas of the timer, and we call warningNoPlug()
      //to say to the user that he had exceed time to plug
      radialID = setInterval(function () {
        timerValue = timerValue-1;
          radialObj.value(timerValue);
          if (timerValue == 0 && timerToZero == false) {
            timerToZero = true;
            $('#radialTimer > canvas').remove();
            existTimer = false;
            warningNoPlug();
          }
      }, 1000);
    }
  }

  //Function that inform the user that he had exceed time to plug his cable(s)
  //We make a modal to apear, with plain text informations, and a new timer
  //modelized by a classical progressbar, with 5 minutes remaining to re-launch
  //the radial timer
  function warningNoPlug()
  {
    var progressValue = 100;
    var counter = 301;
    var progressbar = $("#progressbar");
    $("#recapitulativContent").css("filter", "grayscale(100%)");
    var buttons = {};

    buttons[selectedLanguage["warnReload"]] = function() {
    clearInterval(timerId);
    existTimer = false;
    setTimer();
    $("#recapitulativContent").css("filter", "initial");
    $( this ).dialog( "close" );
    }

    //Enable progressbar inside the div #progressbar
    $( function() {
      $( "#progressbar" ).progressbar({
        value: false
      });
    } );

    //Set time interval, and stock it inside a variable to close it later
      timerId = setInterval(function () {
      counter--;
      progressValue = progressValue-0.3333333333333333333333333;
      progressbar.progressbar( "option", {
        value: progressValue
      });

    //Enable the modal
      $( "#dialog-message" ).dialog({
        modal: true,
        resizable: false,
        draggable: false,
        title: selectedLanguage["noPlugtitle"],
        buttons: buttons
      });

      //Set a 0 before minutes if minutes is lower 10, to make it more lisible
      var minutes = counter % 60;
      if (minutes < 10) {
        minutes = "0" + minutes;
      }
      else {
        minutes = minutes;
      }
      var str = parseInt(counter / 60) + ':' + (minutes);

      //If the counter fall back to 0, we delete Transaction
      //and go back to the main page
      if (counter == 0) {
        clearInterval(timerId);
        $( "#validationSelectionButton" ).removeClass("inActive");
        $( "#nicols" ).removeClass("inActive");
        $( "#dialog-message" ).dialog("close");
        $("#recapitulativContent").css("filter", "initial");
        changeWindowContent("init");
      }
      $("#labTimer").text(str);
    }, 1000);
  }

  //Display a pop-up to confirm the end of the transaction on this connector
  //The user can abort too , and in function if the connector is a tap or not, display a button pause
  function warningConfirm(elementGroup,element)
  {
    var buttons = {};

    buttons[selectedLanguage['warnEnding']] = function() {
        $( this ).dialog( "close" );
        endTransaction(elementGroup,element);
      }

    buttons[selectedLanguage['warnCancel']] = function() {
      $( this ).dialog( "close" );
    }

  $("#dialog-confirm").removeClass("hidden");
    //Enable confirm modal
    $( function() {

  $( "#dialog-confirm" ).dialog({
    resizable: false,
    height: "auto",
    width: 400,
    title: selectedLanguage["confirmTitle"],
    modal: true,
    buttons: buttons,
    open: function(event, ui)
    {
      //We set a timeout to automatically close the dialog after 30 seconds
    setTimeout("$('#dialog-confirm').dialog('close')",30000);
      }
    });
  } );
  }


  function warningWrongCode()
  {
    $( function() {
  $( "#dialog-wrong-code" ).dialog({
    resizable: false,
    height: "auto",
    width: 300,
    title: selectedLanguage["wrongTitle"],
    modal: true,
    open: function(event, ui)
    {
      //We set a timeout to automatically close the dialog after 30 seconds
    setTimeout("$('#dialog-wrong-code').dialog('close')",3000);
    changeWindowContent("init");
    }
    });
  });
  }

//Display a pop-up for ending or re-deliver water
//Similary to the warningConfirm()

function pausedWater(element, elementGroup)

{
  var buttons = {};
  buttons[
    selectedLanguage['warnEnding']] = function() {
      $( this ).dialog( "close" );
      endTransaction(elementGroup,element);
    }

    buttons[selectedLanguage['warnReload']] = function() {
      ipc.send("launchDelivery", $("#"+element).index());
      // waitTimerValidation(5,element);
      $( this ).dialog( "close" );
      $("#"+element).removeClass("paused");
      $("#"+element).addClass("charging");
      waterDroping("#" + $("#"+element).find('.waterDrop').attr("id"));
    }

  $("#dialog-paused").removeClass("hidden");
    $( function() {
  $( "#dialog-paused" ).dialog({
    resizable: false,
    height: "auto",
    width: 400,
    title: selectedLanguage["pausedTitle"],
    modal: true,
    buttons: buttons,
    open: function(event, ui)
    {
    setTimeout("$('#dialog-paused').dialog('close')",30000);
    }
  });
} );
}

//Function that end a transaction connector by connector
//Check if the connector is the last of the transaction to be disconnected
//If so, end the transaction completely and erase it from the existingTransaction
function endTransaction(elementGroup,element)
{
  var withoutCharging = 0;
  $("#"+element).stop();
  $("#"+element).finish();
  $("#"+element).css({ 'background-color' : '', 'opacity' : '' });
  ipc.send("stopDelivery", $("#"+ element).index());


  if ($("#"+ element).index() == 0 || $("#"+ element).index() == 3) {
    const type2Waiting = () => {
     waitForEndingType2();
    ipc.removeListener("type2Waiting", type2Waiting);
    };
    ipc.on("type2Waiting", type2Waiting);
  }
  if ($("#"+element).closest("div").find('img.tap').length !== 0)
  {
      $("#"+element).removeClass("charging");
    if (element == "fifthCircle")
    {
      $("#secondDrop").stop();
      $("#secondDrop").finish();
      $("#secondDrop").css({ 'top' : '', 'left' : '' , 'width' : '', 'height': ''});
      $("#secondTap").addClass("hidden");
    }
    else if (element == "sixthCircle")
    {
      $("#firstDrop").stop();
      $("#firstDrop").finish();
      $("#firstDrop").css({ 'top' : '', 'left' : '' , 'width' : '', 'height': ''});
      $("#firstTap").addClass("hidden");
    }
  }
  if ($("#"+element).hasClass("paused"))
  {
    $("#"+element).removeClass("paused");
    $("#"+element).addClass("unavailable");
  }
  else
  {
    $("#"+element).addClass("unavailable");
  }
  for (var i = 0; i < elementGroup.length; i++) {

    if ($("#"+elementGroup[i]).hasClass("charging") && elementGroup[i] == element)
    {
      $("#"+elementGroup[i]).removeClass("charging");
    }
    if (!$("#"+elementGroup[i]).hasClass('charging') ) {
      withoutCharging = withoutCharging +1;
    }
    if (withoutCharging == elementGroup.length) {
      for (var j = 0; j < elementGroup.length; j++)
      {
        indexToIPC.push($("#"+elementGroup[j]).closest("div").index())

        $("#"+elementGroup[j]).removeClass("unavailable");
        $("#"+elementGroup[j]).stop();
        $(""+elementGroup[j]).finish();
        $("#"+elementGroup[j]).css({ 'background-color' : '', 'opacity' : '' });
        $("#"+elementGroup[j]).removeClass("paused");
          if (elementGroup[j] == "fifthCircle")
          {
            $("#secondDrop").stop();
            $("#secondDrop").finish();
            $("#secondDrop").css({ 'top' : '', 'left' : '' , 'width' : '', 'height': ''});
            $("#secondTap").addClass("hidden");
          }
          else if (elementGroup[j] == "sixthCircle")
          {
            $("#firstDrop").stop();
            $("#firstDrop").finish();
            $("#firstDrop").css({ 'top' : '', 'left' : '' , 'width' : '', 'height': ''});
            $("#firstTap").addClass("hidden");
          }
      }
      ipc.send("endTransaction",indexToIPC);
      indexToIPC = [];
      changeWindowContent("init",1);
      $( "#dialog-ending" ).dialog(
      {
          modal: true,
          resizable: false,
          title: selectedLanguage["endingTitle"],
          draggable: false,
          open: function(event, ui)
          {
            setTimeout("$('#dialog-ending').dialog('close')",3000);
          }
      });
    }
  }
}

  //Removed when mechanical action can validate the transaction
  function validateTransaction()
  {
    $("#validationSelectionButton").removeClass("inActive");
    $('#shipOwnerContent').children().removeClass("inActive");
    $( ".previousArrow" ).removeClass("inActive");

    var previous = null;
    var i = 0;
    tempCode = "";
      $("#iconsBorne > div" ).each(function(index)
      {
        if (i == 2) {
          previous = null;
          i = 0;
        }
        if ($(this).hasClass("selected"))
        {
          $(this).children(".type2").removeClass("hidden");
          $(this).children(".domestic").removeClass("hidden");
          $(this).children(".check").addClass("hidden");
          $(this).removeClass("selected");
          $(this).closest("div").css("background-color","");
          $(this).addClass("charging");
        }
        previous = this;
        i++;
      });

      existingTransaction.push(selectedDevice);
      $('#radialTimer > canvas').remove();
      existTimer = false;
      clearInterval(timerId);
      clearInterval(radialID);
      changeWindowContent("init",1);
  }

  //Function that remove element from an array
  function remove(array, element)
  {
    const index = array.indexOf(element);
    array.splice(index, 1);
  }

  function waitForEndingType2()
  {
    $( "#dialog-type2" ).dialog(
    {
        modal: true,
        resizable: false,
        title: selectedLanguage["type2Title"],
        draggable: false,
        open: function(event, ui)
        {
          setTimeout("$('#dialog-type2').dialog('close')",7000);
        }
    });
  }
});
