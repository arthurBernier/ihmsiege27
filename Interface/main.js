//Begin declarations
const fs = require('fs');
const electron = require('electron');
const crypto = require('crypto');
const EventEmitter = require('events').EventEmitter;
const Buffer = require('buffer').Buffer;
const net = require('net');
const app = electron.app;
const ipc = electron.ipcMain;
const BrowserWindow = electron.BrowserWindow;
var bufferEvent = new EventEmitter();
var socketEvent = new EventEmitter();
const ENQ = 0x00;
const NAK = 0x01;
const SYN = 0x02;
const SEND = 0x02;
const MajorVersion = 1;
const MinorVersion = 0;
var waitingOrders = [];
var sha512 ;
// var MessageHandshake = '{ "Messages" : [' +
// '"checkCode",' +
// '"launchDelivery",' +
// '"validateTransaction",' +
// '"stopDelivery",'+
// '"endTransaction",'+
// '"door",'+
// '"enterAdmin"]}';

// Pour le test
const os = require('os');

//Défini l'usage memoire et autre de l'application
console.log("Usage app: ",app.getAppMetrics());

//End Declarations

/////////Server dialog \\\\\\\\\\\\\


const server = net.createServer((c) => {

  c.pipe(c);
  c.write('hello\r\n');
  // 'connection' listener
  console.log('client connected');
  c.on('end', () => {
    c.write('bye\r\n');
    console.log('client disconnected');
  });
  c.on('data', function(data) {
      // if (data.toString().trim() == "ok") {
      //   mainWindow.webContents.send('checkedCode');
      // }
      // else if (data.toString().trim() == "not") {
      //   mainWindow.webContents.send('uncheckedCode');
      // }
  });
});

server.on('error', (err) => {
  throw err;
});

server.listen(8124, () => {

  console.log('server launched');

});
/////////End Server dialog\\\\\\\\\\\\\

/////////Client dialog\\\\\\\\\\\\\

//For Unix Socket
// The good thing
// socketEvent.on('socketAddr', function(addr) {
//   var client = net.createConnection(addr);
//   client.write(dialogFormat(("write present"),SEND,ENQ));
//   waitingOrders.push(["Handshake",1]);
// });

process.argv.forEach((val,index) => {
  if (index == 2)
  {
    socketEvent.emit("socketAddr",val);
  }
});

//In Case of TCP
//Useful in case of tests in windows platform to linux server
//Dialog with Olivier's Soft

var client = new net.Socket();
client.connect(1337,'192.168.254.204', function() {
   client.write(dialogFormat(("write present"),SEND,ENQ));
   waitingOrders.push(["Handshake",1]);
});

client.on('data', function(data){
  bufferParser(data);
});


client.on('end', function(){
  client.connect(1337,'192.168.254.212', function() {
     client.write(dialogFormat(("write present"),SEND,ENQ));
  });
});


//Dialog with Berenger's soft
// var mailManClient = new net.Socket();
// mailManClient.connect(1337,'192.168.254.212', function() {
//    mailManClient.write(MessageHandshake);
// });

/////////End Client dialog \\\\\\\\\\\\\


//////////  IPC Calls by rendering program \\\\\\\\\\
//Example for Berenger's programm implementation : JSON.stringify({launchDelivery: devices})+"\n"

//Stop a delivery to the connector with ID
ipc.on('checkCode', (event,code) => {
    sha512 = crypto.createHash('sha512').update(code).digest("hex");
    mailManClient.write(JSON.stringify({checkCode: sha512})+"\n");
});

//Launch a delivery to the connector with ID
ipc.on('launchDelivery', (event,devices) => {
  if (devices == 3) {
    client.write(dialogFormat(("write 2 1"),SEND,ENQ));
    waitingOrders.push(["launch",2]);
  }
  else if (devices == 2) {
    client.write(dialogFormat(("write 3 1"),SEND,ENQ));
    waitingOrders.push(["launch",3]);
  }
  else {
    client.write(dialogFormat(("write " + devices + " 1"),SEND,ENQ));
    waitingOrders.push(["launch",devices]);
  }
});

//Launch a delivery to the connector with ID
ipc.on('validateTransaction', (event,devices) => {
  mailManClient.write(JSON.stringify({newTransaction: devices})+"\n");
});

//Stop a delivery to the connector with ID
ipc.on('stopDelivery', (event,devices) => {
    if (devices == 3) {
      client.write(dialogFormat(("write 2 0"),SEND,ENQ));
      mainWindow.webContents.send('type2Waiting');
      waitingOrders.push(["stop",2]);
    }
    else if (devices == 0) {
      client.write(dialogFormat(("write 0 0"),SEND,ENQ));
      mainWindow.webContents.send('type2Waiting');
      waitingOrders.push(["stop",0]);
    }
    else if (devices == 2) {
      client.write(dialogFormat(("write 3 0"),SEND,ENQ));
      waitingOrders.push(["stop",3]);
    }
    else {
      client.write(dialogFormat(("write " + devices + " 0"),SEND,ENQ));
      waitingOrders.push(["stop",devices]);
    }
});


ipc.on('tmpCode', (event,pass) => {
  var lineReader = require('readline').createInterface({
  input: require('fs').createReadStream('./tmp/pass.txt')
  });

  var check = false;
  lineReader.on('line', function (line) {
    if (line == pass) {
      check = true;
    }
  });

  lineReader.on('close', () => {
    if (check == true)
    {
    mainWindow.webContents.send('checkedCode');
    }
    else
    {
      mainWindow.webContents.send('uncheckedCode');
    }
  });
});


//End a transaction with ID
ipc.on('endTransaction', (event,devices) => {
  // mailManClient.write(JSON.stringify({endTransaction: devices})+"\n");
});

//Open or close the door
ipc.on('door', (event,action) => {
    client.write(dialogFormat(("write 7 " + action),SEND,ENQ));
    if (action == 1) {
      waitingOrders.push(["launch",7]);
    }
    else {
      waitingOrders.push(["stop",7]);
    }
});

//Get the enter in admin mode, and the timeStamp attached to it
ipc.on('enterAdmin', (event,timeStamp,codeToHash) => {
  sha512 = crypto.createHash('sha512').update(codeToHash).digest("hex");
  mailManClient.write(dialogFormat((JSON.stringify({adminLog: timeStamp, adminCode: sha512})+"\n"),SEND,ENQ));
});

////////// End of the IPC Calls by rendering program \\\\\\\\\\

////////Creation of the renderer
let mainWindow;
function createWindow ()
{
  //Initializing the main window
  mainWindow = new BrowserWindow(
  {
    width: 600,
    height: 800,
    // frame: false,
    // fullscreen: true,
    // minWidth: 600,
    // maxWidth: 600,
    // minHeight: 800,
    // maxHeight: 800
  });

  mainWindow.webPreferences = {
      WebviewTag : false,
      nativeWindowOption : true
  };
  //We load the html page that permit the renderer
  mainWindow.loadURL(`file://${__dirname}/html/index.html`);

  mainWindow.on('closed', () => {
    mainWindow = null;
  });
}
////////End of creation of the renderer

///////Actions on App

//When the app is ready, we create the window
app.on('ready', createWindow);

//If we detect that all the windows are be closed, we quit the main app
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    //Redémarrage, utile après une maj, ou pour tout autre raison le demandant.
    // app.relaunch({args: process.argv.slice(1).concat(['--relaunch'])})
    // app.exit(0)

    app.quit();
  }
});
///////End Actions on App


// *******************************************************
//
//                   Work in Progress
//
// ********************************************************
function bufferParser(buffer)
{
  var buffSize = parseInt(buffer.toString('hex',7,8), 16);
  var messageSlice = buffer.slice(8,8+buffSize);
  var separator = messageSlice.indexOf(' ');
  var firstComponent = messageSlice.toString('utf8',0,separator).substr(0,separator).trim();
  var secondComponent = messageSlice.toString('utf8',separator,8+buffSize).trim();
  console.log("firstComponent:", firstComponent);
  console.log("secondComponent: ",secondComponent);
  console.log("Parse:",messageSlice);
  bufferEvent.emit("bufferSliced", buffSize);
  checkBufferResponse(firstComponent, secondComponent);
  console.log("_________________________");
}

function checkBufferResponse(firstComponent,secondComponent)
{
  if (secondComponent == "output_on") {
    treatWaitingOrders("launch", firstComponent);
  }
  else if (secondComponent == "output_off") {
    treatWaitingOrders("stop", firstComponent);
  }
  else if (secondComponent == "present")
  {
    treatWaitingOrders("Handshake",1);
  }
}

function dialogFormat(message,type,status)
{
  var length;
  var statusBuffer;
  if (message.length <255)
  {
     statusBuffer = Buffer.from([status,0,0,0,message.length],'hex');
  }
  else
  {
    statusBuffer = Buffer.from([status,0,0,message.length],'hex');
  }
  var firstCompoBuffer = Buffer.from([MajorVersion,MinorVersion,type],'hex');
  var messageBuffer = Buffer.from(message,"utf-8");
  var formatedQuery = Buffer.concat([firstCompoBuffer,statusBuffer,messageBuffer]);
  return formatedQuery;
}

function treatWaitingOrders(typeElement,elementWaiting)
{
  waitingOrders.forEach(function(element,i)
  {
    if (element[0] == typeElement) {
      if (element[1] == elementWaiting[3]) {
        waitingOrders.splice(i,1);
      }
      else {
        if (typeElement == "Handshake") {
            waitingOrders.splice(i,1);
        }
      }
    }
  });
}

// ********************************************************
//
//                     End WIP
//
// *********************************************************
